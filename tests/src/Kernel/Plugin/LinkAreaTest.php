<?php

namespace Drupal\Tests\views_linkarea\Kernel\Plugin;

use Drupal\Tests\views\Kernel\ViewsKernelTestBase;
use Drupal\user\Entity\User;
use Drupal\views\Tests\ViewTestData;
use Drupal\views\Views;

/**
 * Tests the page display plugin.
 *
 * @group views
 * @see \Drupal\views_linkarea\Plugin\views\area\Link
 */
class LinkAreaTest extends ViewsKernelTestBase {

  /**
   * A test entity id.
   *
   * @var int
   */
  protected $entityId;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'user',
    'views_linkarea',
    'views_test_linkarea',
  ];

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_entity_linkarea'];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp();
    if ($import_test_views) {
      ViewTestData::createTestViews(get_class($this), ['views_test_linkarea']);
    }
    $random_label = $this->randomMachineName();
    $data = ['bundle' => 'entity_test', 'name' => $random_label];
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $manager */
    $manager = $this->container->get('entity_type.manager');
    $entity_test = $manager->getStorage('entity_test')->create($data);
    $entity_test->save();
    $this->entityId = (int) $entity_test->id();
    \Drupal::state()
      ->set('entity_test_entity_access.view.' . $this->entityId, TRUE);

    // Setup an anonymous user for our tests, else they will fail.
    User::create([
      'name' => '',
      'uid' => 0,
    ])->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function setUpFixtures(): void {
    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test');
    $this->installConfig(['entity_test']);

    parent::setUpFixtures();
  }

  /**
   * Tests the area handler.
   *
   * @param array $options
   *   Options for the view.
   * @param string $expected_text
   *   The expected text output.
   * @param string $expected_link
   *   The expected link output.
   * @param bool $no_dest
   *   Disables or enables destination parameter.,.
   *
   * @dataProvider providerTestLinkArea
   */
  public function testLinkArea(array $options, $expected_text, $expected_link, $no_dest = TRUE): void {
    if ($no_dest) {
      // Set destination query string to off.
      $options['destination'] = 0;
    }
    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView('test_entity_linkarea');
    $display = $view->getDisplay();
    /** @var \Drupal\views_linkarea\Plugin\views\area\Link $plugin */
    $plugin = Views::pluginManager('area')->createInstance('linkarea');
    // Initialize the plugin.
    $plugin->init($view, $display, $options);
    $build = $plugin->render();
    $this->assertEquals($expected_text, $build['#title']);
    $this->assertEquals($expected_link, $build['#url']->toString());
  }

  /**
   * Data fixture for the test.
   *
   * @return array
   *   A data array for testing.
   */
  public static function providerTestLinkArea() {
    $data = [];
    $data[] = [
      [
        'path' => '<front>',
        'link_text' => 'SSSSSS',
      ],
      'SSSSSS',
      '/',
    ];
    $data[] = [
      [
        'path' => 'route:user.pass',
        'link_text' => '<b>Pass</b>',
      ],
      'Pass',
      '/user/password',
    ];
    $data[] = [
      [
        'path' => 'entity:entity_test/1',
        'link_text' => '<b>Pass</b>',
      ],
      'Pass',
      '/entity_test/1',
    ];
    return $data;
  }

}
