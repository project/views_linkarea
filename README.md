# Views Link Area

The Views Link Area module provides a "link" plugin for Views.
It allows customized links to be placed in the header, footer,
or empty text of any view.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_linkarea).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_linkarea).


## Table of contents

- Requirements
- Installation
- Configuration


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the Views Link Area module.
2. Navigate to *Structure > Views*, and select the view where you want to add a link. 
3. Click "Add" within the **Header**, **Footer**, or **No Results Behavior** sections.
4. Choose *Link* (in the **Global** category with description 
   **Provide an internal or external link** ).
5. Fill out the Link settings form, and save.
